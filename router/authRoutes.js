import express from "express";
import passport from "passport";
// import { loginWithGoogle } from '../middleware/passport.js';
import dotenv from "dotenv";
import process from "process";
dotenv.config();

export const authRoutes = express.Router();
const url = "/api/"+process.env.VERSION+"/";


// Initiates the Google OAuth 2.0 authentication flow
authRoutes.get("/auth/google", passport.authenticate("google", { scope: ["profile", "email"] }));

// Callback URL for handling the OAuth 2.0 response
authRoutes.get("/auth/google/callback", passport.authenticate("google", 
  { failureRedirect: url+"failed" }), 
  (req, res) => {
    res.redirect(url+"success");  
  
});

// Success route if the authentication is successful
authRoutes.get("/success", (req, res) => {
   
  if(req.user) {
    console.log("You are logged in");
    res.redirect( process.env.URL_FRONT + "?token=" + req.user.token);
  } else {
    res.redirect( url + "failed");
  }
 
});

// Logout route
authRoutes.get("/logout", (req, res) => { 
  req.session.destroy((err) => {
    if (err) {
        console.log("Error while destroying session:", err);
    } else {
        req.logout(() => {
            console.log("You are logged out");
            res.redirect("/");
        });
    }
  });
});

// failed route if the authentication fails
authRoutes.get("/failed", (req, res) => {
  console.log("User is not authenticated");
  res.redirect( process.env.URL_FRONT +"?user=null");
});

