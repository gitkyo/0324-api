import process from "process";

//During the test the env variable is set to test
process.env.NODE_ENV = "test";
const url = process.env.BASE_URL + process.env.VERSION;

import chai from "chai";
import chaiHttp from "chai-http";
import { it } from "mocha";
import { app } from "./../index.js";
import { describe } from "mocha";
// import { getAllUser } from "../controllers/user.js";
let should = chai.should();
app.use(chaiHttp);

chai.use(chaiHttp);
describe("init", () => {
    it("test home page of server", (done) => {
        //used to send a HTTP request to the app.
        chai.request(url)
            //get method is used to specify the endpoint
            .get("/")
            //end function is used to handle the response
            .end((_, res) => {
                should.not.exist(_);
                //assertion checks if the response status is 200
                res.should.have.status(200);
                //The done function is called to indicate that the test is complete.
                done();
            });
    });
});

/*
* Test the /login route with good credentials like kuku@kuku.com and 12345 password
*/
let token = "";
describe("/login", () => {
    it("test login form", (done) => {
        chai.request(url)
            .post("/login")
            .send({ email: process.env.USER_TEST_EMAIL, password: process.env.USER_TEST_PASSWORD })
            .end((_, res) => {
                token = res.body.token;
                res.should.have.status(200);
                done();
            });
    });
});

/*
* Test the /login route with wrong credentials like kuku@kuku.com and 123456 password
*/
describe("/login", () => {
    it("it should GET code 400", (done) => {
        chai.request(url)
            .post("/login")
            .send({ email: "looo@firiri.com", password: "qsdhiaz" })
            .end((_, res) => {
                res.should.have.status(400);
                done();
            });
    });
});


/*
* Unit Testing :  the getAllUser function
*/
// describe("Get all users function", () => {
//     it("test the getAllUser function", (done) => {
//         getAllUser(null, {
//             send: (data) => {
//                 console.log("data : "+data);
//                 // data.should.be.a("object");
//                 done();
//             }
//         });
//     });
// });



/*
* Test the Post /user route with wrong credentials like
*/
describe("/users", () => {
    it("it shouldn't POST a new user with too small password", (done) => {
        chai.request(url)
            .post("/users")
            .send({ nom: "kuku", age: 25, email: "other@test.com", password: "small"})
            .end((_, res) => {               
                res.should.have.status(500);               
                done();
            });
    }   );
});

/*
* Test the Post /user route with wrong credentials like
*/
describe("/users", () => {
    it("it shouldn't POST a new user with too small age", (done) => {
        chai.request(url)
            .post("/users")
            .send({ nom: "kuku", age: 0, email: "other@test.com", password: "verylongerpassword"})
            .end((_, res) => {                
                res.should.have.status(500);               
                done();
            });
    }   );
});

/*
* Test the Post /user route with wrong credentials like
*/
describe("/users", () => {
    it("it shouldn't POST a new user with not unique email", (done) => {
        chai.request(url)
            .post("/users")
            .send({ nom: "kuku", age: 20, email: "kuku@kuku.com", password: "verylongerpassword"})
            .end((_, res) => {
                //get error message
                // console.log(res.error.text);
                res.should.have.status(500);               
                done();
            });
    }   );
});



/*
*
*
*
* TASKS TESTS
* Todo : include an other file for tasks tests
*
*
*
*/

/*
* Test the Get /tasks with a valid user token
*/
describe("Get all tasks", () => {
    it("get all tasks", (done) => {
        chai.request(url)
            .get("/tasks")
            .set("Authorization", "Bearer " + token)
            .end((_, res) => {
                res.should.have.status(200);
                done();
            });
    });
});

/*
* Test the Post /tasks with a valid user token
*/
let taskId = 0;
describe("Post a tasks", () => {
    // console.log("token: " + token);
    it("add a task", (done) => {
        chai.request(url)
            .post("/tasks")
            .set("Authorization", "Bearer " + token)
            .send({ 
                description: "task 1",
                completed: false,
            })
            .end((_, res) => {
                res.should.have.status(201);
                //save task id to edit and delete it in nexts requests
                taskId = res.body.id;
                done();
            });                      

        });
});  

/*
* Test the Get /tasks by id with a valid user token
*/
describe("Get a task by id", () => {
    it("get a task by id", (done) => {
        chai.request(url)
            .get("/tasks/"+taskId)
            .set("Authorization", "Bearer " + token)
            .end((_, res) => {
                res.should.have.status(200);
                done();
            });
    });
});



/*
* Test the Put /tasks with a valid user token and a valid task id
*/
describe("Put a tasks", () => {
    it("edit a task", (done) => {
        chai.request(url)
            .put("/tasks/" + taskId)
            .set("Authorization", "Bearer " + token)
            .send({ 
                description: "task 1 edited",
                completed: true,
            })
            .end((_, res) => {
                res.should.have.status(204);
                done();
            });
    });
});

/*
* Test the Delete /tasks with a valid user token and a valide task id
*/
describe("Delete a tasks", () => {
    it("delete a task", (done) => {
        chai.request(url)
            .delete("/tasks/" + taskId)
            .set("Authorization", "Bearer " + token)
            .end((_, res) => {
                res.should.have.status(200);
                done();
            });
    });
});







   