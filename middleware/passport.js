import passport from "passport";
import GoogleStrategy from "passport-google-oauth20"; 
import dotenv from "dotenv";
dotenv.config();
import process from "process";
import {User} from "../models/user.js";
//https://codeculturepro.medium.com/implementing-authentication-in-nodejs-app-using-oauth2-0-59fee8f63798
//https://medium.com/@alysachan830/cookie-and-session-ii-how-session-works-in-express-session-7e08d102deb8
//try here : http://localhost:3000/auth/google
passport.use(
  new GoogleStrategy( 
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.GOOGLE_CALLBACK_URL,      
    },
    (accessToken, refreshToken, profile, done) => {
      // Code to handle user authentication and retrieval
      console.log("Code to handle user authentication and retrieval" );

      return done(null, profile); 
    }
  )
);

passport.serializeUser((user, done) => {
  // Code to serialize user data 
  done(null, user._json.email);
});

passport.deserializeUser(async (email, done) => {

  
  // Code to deserialize user data
  
  //test if user exist in database
  const user = await User.findOne({
    where: {
        email: email
    }
  });  

  //redirect to /failed if user not found
  if(!user) {
    console.log("User not found");
    done(null, false);    
  }else{  
    //done will redirect to the route /success
    done(null, user);  
  }
 

});


//export passport
export default passport;
