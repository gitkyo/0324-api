# 0324-api

A learning project - API Rest of a Todo-list App with a MySQL database and nodeJS & ExpressJS.

## demo here

[Render](https://api-0324.onrender.com/api/0.2.28/).

## install all dependance

```npm install```

### to start projet

```npm run start```

### to run in dev mode

```npm run dev```

### to run test

```npm run test```

### run doc generation

```apidoc -i ./ -o public/apidoc```

### postMan

You can import a Postman collection example on "0324 - MYSQL API.postman_collection.json"

### database

You can find an export of database example on 0324.sql.zip

### env variable

You need to create a .env file with some infos :

DB_USERNAME='your-username-database'
DB_PASSWORD='your-password-database'
DB_HOST='your-host-database'
DB_DATABASE='your-name-database'
DB_DIALECT='your-driver-type-database'
JWT_KEY='your-jwt-secret-phrase'

## Front end exemple with react

<https://gitlab.com/gitkyo/0324-api-react-2>

<https://react-front-api-0324.onrender.com/>

### todo

- change some code with typeScript like connection databases & singleton
- edit and test dockerfile
- Add conJob
- add log tool
- complete unit test collection
- test playright
- improve perf / sécurity best practices like add httpOnly header : <https://dev.to/bcerati/les-cookies-httponly-une-securite-pour-vos-tokens-2p8n>
