/**
 * Fichier principal de l'application
*/
import {customApiController} from "./controllers/custom-api.js";
import helmet from "helmet";
import { RateLimiterMemory } from "rate-limiter-flexible";
import bodyParser from "body-parser";
import apicache from "apicache";
import cors from "cors";
import process from "process";

//import log
import { logger } from "./log.js";

// Debut de mon serveur express
import express from "express"; 

// Je crée une instance de mon serveur express
export const app = express();
 
//cors - Access-Control-Expose-Headers: Access-Token, cookie
// app.use(cors())

app.use(cors({
    // origin: process.env.BASE_URL,
    // credentials: true,
    origin: "*",
    credentials: "include",
    AccessControlExposeHeaders: ["Access-Token", "set-cookie"],
    AllowedHeaders: ["Access-Token", "set-cookie"],
    //expose cookie
}));

//get node env
const env = process.env.NODE_ENV || "development";

if (env === "production") {
    //get env file
    //here we cached all routes
    let cache = apicache.middleware;
    app.use(cache(1000));
}

 
// set the request size limit to 1 MB
app.use(bodyParser.json({ limit: "1mb" }));

// Use Helmet to avoid security issues with unsafe-eval option
app.use(
    helmet.contentSecurityPolicy({
        directives: {
          "default-src": ["'self'"],
        //   "connect-src": ["'self'", "'unsafe-inline'"],
          "img-src": ["https://http.cat/images/404.jpg 'self'", "data:"],
        //   "style-src-elem": ["'self'", "data:"],
            //script src with authorization on http://localhost:3000/apidoc/index.html
          "script-src": ["'self'", "'unsafe-eval'"],        
          "object-src": ["'none'"],
          "script-src-elem": ["'self'", "'unsafe-eval'"]
          
            
        },
      })
);

//rate limiter to avoid brute force attack
if (env === "production") {    
    const rateLimiter = new RateLimiterMemory({
        points: 10, // maximum number of requests allowed
        duration: 1, // time frame in seconds
    });
    const rateLimiterMiddleware = (req, res, next) => {
        rateLimiter.consume(req.ip)
        .then(() => {
            // request allowed, 
            // proceed with handling the request
            next();
        })
        .catch(() => {
            // request limit exceeded, 
            // respond with an appropriate error message
            res.status(429).send("Too Many Requests");
        });
    };
    app.use(rateLimiterMiddleware);
}

//indiquer a express qu'on peut insérer des donnée au format json !
app.use(express.json());

//indiquer a express qu'on peut insérer des donnée au format form data
app.use(express.urlencoded({ extended: true }));

//set var to get path directory
import { fileURLToPath } from "url";
import { dirname } from "path";
import path from "path";
const __filename = fileURLToPath(import.meta.url);
export const __dirname = dirname(__filename);
const publishDirectoryPath = path.join(__dirname, "./public");

//set public directory
app.use(express.static(publishDirectoryPath));

//indiquer a express qu'on peut utiliser le moteur de template ejs
app.set("view engine", "ejs"); 

// set url with version
const url = "/api/"+process.env.VERSION+"/";

//here exemple with https://www.npmjs.com/package/express-session 
//bug in production : https://stackoverflow.com/questions/44882535/warning-connect-session-memorystore-is-not-designed-for-a-production-environm
import session from "express-session";
import passport from "./middleware/passport.js";
app.use(session({ 
    secret: "YOUR_SESSION_SECRET", 
    name : "session",
    resave: false, 
    saveUninitialized: false 
}));
app.use(passport.initialize());
app.use(passport.session());

// TODO : Check here : https://blog.bitsrc.io/implementing-google-oauth2-authentication-in-node-js-project-using-passport-a-step-by-step-guide-c5e38e9f6071

//here exemple with https://www.npmjs.com/package/cookie-session
/*import cookieSession from 'cookie-session'
import passport from './middleware/passport.js'
app.use(passport.initialize());
app.use(cookieSession({
    name: 'session',
    keys: ["secret keys"],
    resave: false,
    
    // Cookie Options
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
  }))
*/
//import des routes
import {authRoutes} from "./router/authRoutes.js";
import {taskRouter} from "./router/task.js";
import {userRouter} from "./router/user.js";

//indiquer a espress qu'on utilise un routeur
app.use(url, taskRouter); 
app.use(url, userRouter);
app.use(url, authRoutes);

//add compression to express
import compression from "compression";
app.use(compression()); 


//route get sur l'url /
app.get(url, (req, res) => {
    
    // envoi du texte hello world
    // res.send('Hello World!')

    //envoi de json
    // res.json({message: 'Hello World!'})

    //envoi de html
    // res.send('<h1>Hello World!</h1>') 

    res.render("index", {title: "Accueil"});

});

//route /api pour afficher une api distante que j'ai altéré
app.get("/custom-api", async (req, res) => {
    customApiController(req, res);  
});

//middleware pour la page 404
app.use((req, res) => {
    res.status(404).send(
        "<style>body{background: url(https://http.cat/images/404.jpg) no-repeat center center fixed #000000;}</style>");
});

//demarrage du serveur

app.listen(process.env.PORT, () => {
    logger.log({
        level: "info",
        message: `Example app listening at ${process.env.BASE_URL+process.env.VERSION} on ${env} environment`
      });
    // console.log(`Example app listening at ${process.env.BASE_URL+process.env.VERSION} on ${env} environment`);
});  







